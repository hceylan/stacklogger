package org.batoo.util.stacklogger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.util.Arrays;

/**
 * <p>
 * A Utility class to dump unique stack traces to file system. The whole
 * implementation is static.
 * <p>
 * Unless the root dir is specified, it creates a directory in the home
 * directory name MyStackTraces and a directory with the pid in it. Stack traces
 * are dumped the traces into that directory. For any reason home directory
 * cannot be located then the tmp directory is used.
 * 
 * <p>
 * When the program starts, the directory is not purged.
 * 
 * <p>
 * The implementation is written on Linux with sun jdk 6. For platform support
 * try with main on the target platform.
 * 
 * @author hceylan
 * 
 */
public class StackLogger {

	private static final String EXTENSION = ".log";

	private static final String DIR_ROOT = "MyStacks/";

	private static final String SYS_USER_HOME = "user.home";

	private static final String SYS_TMP_DIR = "java.io.tmpdir";

	private static final String CHAR_SLASH = "/";

	private static final String DUMP_LOCATION = getDefaultLocation();

	private static final String DEF_TMP_DIRECTORY = "/tmp/";

	// INITIALIZATION - if fails on any platform, modify accordingly.

	private static String getDefaultLocation() {
		String dir = getDefaultLocationImpl();

		if (!dir.endsWith(CHAR_SLASH)) {
			dir = dir + CHAR_SLASH;
		}

		dir = dir + DIR_ROOT;

		String pid = tryToGetPid(dir);
		if (pid != null) {
			dir = dir + pid;
		}

		System.err
				.println("\n\n\n>>>>>>>> Unless you call logStack(String) all dumps will go into \""
						+ dir + "\" <<<<<<<<<<\n\n\n");

		return dir;
	}

	/**
	 * Try to get the pid of the process
	 * 
	 * @param dir
	 * @return
	 */
	private static String tryToGetPid(String dir) {
		try {
			String pid = ManagementFactory.getRuntimeMXBean().getName();
			if (pid != null && pid.length() > 0) {
				if (!pid.endsWith(CHAR_SLASH)) {
					pid = pid + CHAR_SLASH;
				}
			}

			return pid;
		} catch (Throwable e) {
			// fallback
			return dir;
		}
	}

	/**
	 * Get the default location
	 * 
	 * @return
	 */
	private static String getDefaultLocationImpl() {
		String dir = System.getProperty(SYS_USER_HOME);
		if (dir != null && dir.length() > 0) {
			return dir;
		}

		dir = System.getProperty(SYS_TMP_DIR);
		if (dir != null && dir.length() > 0) {
			return dir;
		}

		try {
			dir = new File(".").getCanonicalPath().toString();
			if (dir != null && dir.length() > 0) {
			}
		} catch (IOException e) {
		}

		System.err.println(">>>>>>>> Stack dumps will go into "
				+ DEF_TMP_DIRECTORY + "...! <<<<<<<<<<");

		return DEF_TMP_DIRECTORY;
	}

	private StackLogger() {
		// No instantiation
	}

	// VERIFICATION

	/**
	 * For testing purposes. if there is an argument then that directory is used
	 * as the base directory.
	 * 
	 * @param args
	 *            a single param as path or nothing for default mechanism.
	 */
	public static void main(String[] args) {
		if (args.length > 1) {
			usage();
		}

		String dir = DUMP_LOCATION;
		if (args.length == 1) {
			dir = args[0];
			System.err.println(dir
					+ " has been used for testing, default location "
					+ DUMP_LOCATION + " will not be used in that test!!!!!!");
		}

		System.err
				.println("Unless the source is reformatted, you should see two dumps not three. "
						+ "\nIf you see three dumps check that the source file is not formatted");

		// the following first two dumps must exist on the same line to imitate same trace from different sources
		// due to auto formating they may go into three lines and deceive
		// the probe test.
		logStack(dir);	logStack(dir);
		logStack(dir);

		System.exit(0);
	}

	private static void usage() {
		System.err.println("Usage:\n\n");
		System.err
				.println("java -jar stacklogger.jar [DIRECTORY]");
		System.exit(1);
	}

	// IMPLEMENTATION - if fails on any platform, modify accordingly.

	/**
	 * Logs the stack to default directory
	 */
	public static void logStack() {
		logStack(DUMP_LOCATION);
	}

	/**
	 * Logs the stack to specified directory. Directory must exist!
	 * 
	 * @param dumpLocation
	 *            the directory to dump into
	 */
	private static void logStack(String dumpLocation) {
		// Check the folder ends with a slash
		if (!dumpLocation.endsWith(CHAR_SLASH)) {
			dumpLocation = dumpLocation + CHAR_SLASH;
		}

		// Create if it doesn't exists.
		File dir = new File(dumpLocation);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				// log the error to stderr and silently return
				new Exception("Cannot create stack dump directory in "
						+ dumpLocation).printStackTrace();

				return;
			}
		}

		// Obtain the trace
		Exception e = new Exception();
		StackTraceElement[] ste = e.getStackTrace();
		int hashCode = Arrays.hashCode(ste);

		// Make sure it is unique by checking the file does now exists
		String fileName = dumpLocation + Integer.toString(hashCode) + EXTENSION;
		File file = new File(fileName);
		if (file.exists()) {
			return;
		}

		// Dump the trace into the file
		try {
			PrintWriter pw = new PrintWriter(file);
			try {
				e.printStackTrace(pw);
			} finally {
				pw.close();
			}
		} catch (Throwable e1) {
			// log the error to stderr and silently return
			new Exception("Cannot dump stack into " + fileName)
					.printStackTrace();
		}
	}
}
